#!/usr/bin/perl
use strict;
use warnings;
use Error qw(:try);

use Compress::LZF;

my $fname="$ARGV[0]";

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

open IDX, "$fname.idx";
open (FD, "$fname.bin") or die "$!";
binmode(FD);
while (<IDX>){
	chop();
        my @x = split(/\;/, $_, -1);
        my ($id, $off, $lC, $sha) = @x;
        if($#x == 6){
            $sha = $x[4];
        }
	my $codeC = "";
	my $rl = read (FD, $codeC, $lC);
	my $code = safeDecomp ($codeC);
	my $len = length ($code);
        my ($tree, $parent, $auth, $cmtr, $ta, $tc) = ("","","","","","");
        my ($pre, @rest) = split(/\n\n/, $code, -1);
        for my $l (split(/\n/, $pre, -1)){
           #print "$l\n";
           $tree = $1 if ($l =~ m/^tree (.*)$/);
           $parent .= ":$1" if ($l =~ m/^parent (.*)$/);
           ($auth, $ta) = ($1, $2) if ($l =~ m/^author (.*)\s([0-9]+\s[\+\-]\d\d\d\d)$/);
           ($cmtr, $tc) = ($1, $2) if ($l =~ m/^author (.*)\s([0-9]+\s[\+\-]\d\d\d\d)$/);
        }
        $parent =~ s/^:// if defined $parent;
	print "$id;$len;$sha;$tree;$parent;$auth;$cmtr;$ta;$tc\n";
}
