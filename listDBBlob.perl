#!/usr/bin/perl
use strict;
use warnings;
use Error qw(:try);

use Compress::LZF;

my $fname="$ARGV[0]";

sub safeDecomp {
        my ($codeC, $n) = @_;
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex; $n\n";
                return "";
        }
}

open IDX, "$fname.idx";
open (FD, "$fname.bin") or die "$!";
binmode(FD);
while (<IDX>){
	chop();
        my @x = split(/\;/, $_, -1);
        my ($id, $off, $lC, $sha) = @x;
        if($#x == 6){
            $sha = $x[4];
        }
	my $codeC = "";
	my $rl = read (FD, $codeC, $lC);
	my $code = safeDecomp ($codeC, "$id;$off;$sha");
	my $len = length ($code);
	print "$id;$off;$sha\n";
        exit (-1) if $len == 0;
}
