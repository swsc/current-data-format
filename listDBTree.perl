#!/usr/bin/perl
use strict;
use warnings;
use Error qw(:try);

use Compress::LZF;

my $fname="$ARGV[0]";

sub safeDecomp {
        my $codeC = $_[0];
        try {
                my $code = decompress ($codeC);
                return $code;
        } catch Error with {
                my $ex = shift;
                print STDERR "Error: $ex\n";
                return "";
        }
}

open IDX, "$fname.idx";
open (FD, "$fname.bin") or die "$!";
binmode(FD);
while (<IDX>){
  chop();
  my @x = split(/\;/, $_, -1);
  my ($id, $off, $lC, $sha) = @x;
  if($#x == 6){
    $sha = $x[4];
  }
  my $codeC = "";
  my $rl = read (FD, $codeC, $lC);
  my $code = safeDecomp ($codeC);
  my $len = length ($code);
  my $treeobj = $code;
  while ($treeobj) {
  # /s is important so . matches any byte!
    if ($treeobj =~ s/^([0-7]+) (.+?)\0(.{20})//s) {
      my($mode,$name,$bytes) = (oct($1),$2,$3);
      printf "%06o;%s;%s\n",
        $mode, #($mode == 040000 ? "tree" : "blob"),
        unpack("H*", $bytes), $name;
    } else {
      die "$0: unexpected tree entry";
    }
  }
}
