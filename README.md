# README #

This README would normally describe the current format of oss data on da servers.

### Format ###
There several versions of the current data, some earlier formats are
described on https://bitbucket.org/swsc/historic.  
The latest format is based on git internal objects i.e. commit, tree, blob, tag.As for now, this format has 2 different structures for different purposes:  

1.  Update/storage optimized
    * (a) da4:/{fast1,fast}/All.sha1 128 tables for sha1 storage of blobs/commits/trees/tags  
    * (b) da4:/data/All.blobs  128 tables for content storage where (a) has record number with new records being appended
    ```
    perl  -I ~audris/lib64/perl5/ ~audris/bin/listDBCmt.perl All.blobs/commit_0
    ```  
    The output fields are:
        * id;
        * length;
        * commit sha;
        * tree sha;
        * parent commit sha;
        * author;
        * committer;
        * author time;
        * commit time;

2. Access/analytics optimized
     *   da4:/fast1/All.sha1c/commit_[0..127].tch tables of commit sha1 to commit content
     *   da4:/fast1/All.sha1c/tree_[0..127].tch tables of tree sha1 to tree content
     *   'Access optimized for blobs' is being created  

Besides, we have data in other formats:

* da4:/data/c2fbp.gz consists of commit sha, file path, blob sha, project name. It only includes repos using git.
* da[3,4]:/data/c2pTrPaT.gz consists of commit sha, project name, tree sha, parent sha, author time
* da[3,4]:/data/b2pf.gz consists of blob sha, project name, file path